from django.db import models
from django.urls import reverse
# Create your models here.
from django.conf import settings
from django.contrib.auth.models import User

class Post(models.Model):
    user = models.OneToOneField(User,default=1, on_delete=models.CASCADE)
    title = models.CharField(max_length=120) 
    content = models.TextField()
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)


    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return "/posts/%s/" %(self.id)
    def get_base_url(self):
        return "/posts/"