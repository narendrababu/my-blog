from django.contrib import admin
from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    path(r'posts/create/', views.posts_create),
    url(r'^posts/(?P<id>\d+)/$', views.posts_detail,name="detail"),
    url(r'^posts/$', views.posts_list),
    url(r'^posts/(?P<id>\d+)/edit/$', views.posts_update),
    url(r'^posts/(?P<id>\d+)/delete/$', views.posts_delete),
]
