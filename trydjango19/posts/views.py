from django.http import HttpResponse ,  HttpResponseRedirect ,Http404
from django.shortcuts import render ,get_object_or_404, redirect

from .forms import PostForm 
from .models import Post
# Create your views here.

def posts_create(request):
    # if not request.user.is_staff or not request.user.is_superuser or not request.user.is_active:
    #     raise Http404
    form  = PostForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return HttpResponseRedirect(instance.get_absolute_url())
    # if request.method == "POST":
    #     print(request.POST.get("content"))
    #     print(request.POST.get("title")

    context={
        "form":form,
    }
    return render(request,"post_form.html",context)

def posts_detail(request,id): #retrieve
    instance = Post.objects.get(id=id)
    # instance = get_object_or_404(Post,id=id)
    context={
        "title" : instance.title,
        "instance":instance
    }
    return render(request,'post_detail.html',context)

def posts_list(request): #list items

    # if request.user.is_authenticated:
    #     context = {
    #         "title" : "my user list"
    #     }
    # else:
    all_post_list = Post.objects.all()
    context = {
        "all_list":all_post_list,
        "title" : "List"
    }
    return render(request,'index.html',context)

def posts_update(request,id):
        instance = get_object_or_404(Post, id=id)
        form = PostForm(request.POST or None, instance=instance)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            return HttpResponseRedirect(instance.get_absolute_url())

        context = {
            "title": instance.title,
            "instance": instance,
            "form":form,
        }
        return render(request, "post_form.html", context)

def posts_delete(request,id):
   instance = get_object_or_404(Post, id=id)
   instance.delete()
   return HttpResponseRedirect(instance.get_base_url())


